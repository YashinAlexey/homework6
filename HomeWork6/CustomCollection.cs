﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace HomeWork6
{
    public class CustomCollection<T> : IEnumerable<T> where T : class
    {
        private T[] _items;

        public CustomCollection(T[] items)
        {
            _items = items;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < _items.Length; ++i)
                yield return _items[i];
        }
    }
}
