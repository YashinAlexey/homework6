﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork6
{
    public static class EnumerableExtension
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter) where T : class
        {
            T tMax = default(T);

            foreach (T t in e)
            {
                if (getParameter?.Invoke(t) > getParameter?.Invoke(tMax))
                    tMax = t;                
            }

            return tMax;
        }
    }
}
