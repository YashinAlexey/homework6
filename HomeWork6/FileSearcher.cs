﻿using System;

namespace HomeWork6
{
    public class FileSearcher
    {
        public event EventHandler<FileArgs> FileFound;
        public event EventHandler<FileArgs> SearchStopped;
        public int MaxFilesCount { get; set; }

        public void FoundFiles(string path)
        {
            
            string[] files = System.IO.Directory.GetFiles(path);
            for (int i = 0; i < files.Length; i++)
            {
                if (i == MaxFilesCount)
                {
                    SearchStopped?.Invoke(this, new FileArgs { Messege = $"Найденo более {MaxFilesCount} файлов" });
                    break;
                }

                FileFound?.Invoke(this, new FileArgs { Messege = $"Найден файл : {files[i]}"});
            }            
        }
    }
}
