﻿using System;
using System.Collections.Generic;

namespace HomeWork6
{
    class Program
    {
        static void Main(string[] args)
        {
            Person[] persons = new Person[] { new Person { Name = "Tom", Height = 40.04f },
                                              new Person { Name = "John", Height = 30.03f },
                                              new Person { Name = "Mary", Height = 20.02f },
                                              new Person { Name = "Anna", Height = 50.05f },
                                              new Person { Name = "Jack", Height = 10.01f }};

            CustomCollection<Person> personsCollection = new CustomCollection<Person>(persons);
            Person personWithMaxHeight = personsCollection.GetMax<Person>(x => (x?.Height ?? 0f));
            Console.WriteLine("Элемент с максимальным значением:" + Environment.NewLine +
                              $"Name = {personWithMaxHeight.Name}" + Environment.NewLine + 
                              $"Height = {personWithMaxHeight.Height}" + Environment.NewLine);

            FileSearcher fileSearcher = new FileSearcher { MaxFilesCount  = 5 };
            fileSearcher.FileFound += OnFileFound;
            fileSearcher.SearchStopped += OnSearchStopped;
            fileSearcher.FoundFiles(Environment.CurrentDirectory);

            Console.ReadLine();
        }        

        private static void OnFileFound(object sender, FileArgs e)
        {
            Console.WriteLine(e.Messege);
        }

        private static void OnSearchStopped(object sender, FileArgs e)
        {
            Console.WriteLine(e.Messege);
        }
    }    
}
